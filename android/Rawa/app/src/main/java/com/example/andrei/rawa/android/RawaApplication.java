package com.example.andrei.rawa.android;

import android.app.Application;

import com.example.andrei.rawa.android.io.login.DummyLoginApi;
import com.example.andrei.rawa.android.io.login.SharedPrefLoggedUserDataStorage;
import com.example.andrei.rawa.use_cases.LoggedUserDataStorage;
import com.example.andrei.rawa.use_cases.login.LoginApi;

import toothpick.Scope;
import toothpick.Toothpick;
import toothpick.config.Module;

public class RawaApplication extends Application {
    public static final String APP_SCOPE = "applciationScope";

    @Override
    public void onCreate() {
        super.onCreate();

        Scope scope = Toothpick.openScope(APP_SCOPE);
        scope.installModules(new MockEverythingModule());
        Toothpick.inject(this, scope);
    }

    private class MockEverythingModule extends Module {
        MockEverythingModule() {
            bind(LoggedUserDataStorage.class).toInstance(new SharedPrefLoggedUserDataStorage(RawaApplication.this));
            bind(LoginApi.class).to(DummyLoginApi.class);
        }
    }
}
