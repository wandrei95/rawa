package com.example.andrei.rawa.android.io;

import android.util.Log;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;

import java.lang.reflect.Type;

public class GsonHelper {
    private final Gson mGson;

    public GsonHelper() {
        mGson = new GsonBuilder().setFieldNamingStrategy(FieldNamingPolicy.IDENTITY).create();
    }

    public String tojson(Object object) {
        return mGson.toJson(object);
    }

    public <T> T fromJson(String string, Type type) {
        try {
            return mGson.fromJson(string, type);

        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        }

        return null;
    }
}
