package com.example.andrei.rawa.android.io.login;


import com.example.andrei.rawa.entities.ErrorCode;
import com.example.andrei.rawa.use_cases.UseCaseOperation;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseOperation<Params, Result> implements UseCaseOperation<Params, Result> {
    protected UseCaseOperationListener<Result> listener;
    protected List<Result> unhandledResults = new ArrayList<>();
    protected List<ErrorCode> unhandledErrors = new ArrayList<>();

    @Override
    public void unbindListener() {
        listener = null;
    }

    @Override
    public void bindListener(UseCaseOperation.UseCaseOperationListener<Result> listener) {
        this.listener = listener;
        if (hasUnhandledResults()) {
            for (Result result : getUnhandledResults()) {
                listener.onOperationSuccess(result);
            }
            clearUnhandledResults();
        }
        if (hasUnhandledErrors()) {
            for (ErrorCode errorCode : getUnhandledErrorss()) {
                listener.onOperationFailure(errorCode);
            }
            clearUnhandledErrorss();
        }
    }

    private boolean hasUnhandledResults() {
        return unhandledResults.size() > 0;
    }

    private boolean hasUnhandledErrors() {
        return unhandledErrors.size() > 0;
    }

    private void clearUnhandledResults() {
        unhandledResults.clear();
    }

    private void clearUnhandledErrorss() {
        unhandledErrors.clear();
    }

    private List<Result> getUnhandledResults() {
        return unhandledResults;
    }

    private List<ErrorCode> getUnhandledErrorss() {
        return unhandledErrors;
    }
}