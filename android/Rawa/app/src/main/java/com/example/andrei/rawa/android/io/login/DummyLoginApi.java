package com.example.andrei.rawa.android.io.login;

import com.example.andrei.rawa.entities.LoginCredentials;
import com.example.andrei.rawa.use_cases.login.LoginApi;
import com.example.andrei.rawa.use_cases.login.LoginApiListener;

import javax.inject.Inject;

public class DummyLoginApi implements LoginApi {
    @Inject
    public DummyLoginApi(){

    }

    @Override
    public void doLogin(LoginCredentials loginCredentials, LoginApiListener listener) {
        listener.onLoginSuccess(loginCredentials, "ASDFA-SDF1231-23asdfa-23d3d");
    }

    @Override
    public void doLogout(LoginApiListener listener) {
        listener.onLogoutSuccess();
    }
}
