package com.example.andrei.rawa.android.io.login;

import android.net.Credentials;
import android.os.AsyncTask;
import android.util.Log;

import com.example.andrei.rawa.android.io.GsonHelper;
import com.example.andrei.rawa.entities.LoginCredentials;
import com.example.andrei.rawa.use_cases.login.Login;

import java.io.IOException;

import javax.inject.Inject;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class LoginOperation extends BaseOperation<LoginCredentials, Void> {
    @Inject
    public LoginOperation() {

    }

    @Override
    public void start(LoginCredentials credentials) {
        new LoginTask(credentials).execute();
    }

    private class LoginTask extends AsyncTask<Void, Void, Void> {
        private LoginCredentials credentials;

        LoginTask(LoginCredentials credentials) {
            this.credentials = credentials;
        }

        @Override
        protected Void doInBackground(Void... params) {
            MediaType JSON = MediaType.parse("application/json; charset=utf-8");

            OkHttpClient client = new OkHttpClient();

            GsonHelper gsonHelper = new GsonHelper();


            //http://192.168.0.106/login.php
            String url = "http://" + "192.168.0.106" + ":80/login.php";
            RequestBody body = RequestBody.create(JSON, gsonHelper.tojson(credentials));
            Request request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .build();
            Response response = null;
            try {
                response = client.newCall(request).execute();
                Log.e("AAAAA", response.body().string());
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}
