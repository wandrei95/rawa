package com.example.andrei.rawa.android.io.login;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.andrei.rawa.entities.LoginCredentials;
import com.example.andrei.rawa.use_cases.LoggedUserDataStorage;

import javax.inject.Inject;

public class SharedPrefLoggedUserDataStorage implements LoggedUserDataStorage {
    private static final String SHARED_PREF = "loggedUserDataStorage";
    private static final String SESSION_ID_KEY = "sessionID";
    private static final String LOGGED_USER_USERNAME = "userName";
    private static final String LOGGED_USER_PASSWORD = "password";

    private Context context;

    @Inject
    public SharedPrefLoggedUserDataStorage(Context context) {
        this.context = context;
    }

    @Override
    public void saveSessionID(String sessionID) {
        SharedPreferences.Editor editor = getSharedPrefEditor();
        editor.putString(SESSION_ID_KEY, sessionID);
        editor.apply();
    }

    @Override
    public void removeCurrentSessionID() {
        SharedPreferences.Editor editor = getSharedPrefEditor();
        editor.remove(SESSION_ID_KEY);
        editor.apply();
    }

    @Override
    public void saveCurrentLoggedUserData(LoginCredentials loginCredentials) {
        SharedPreferences.Editor editor = getSharedPrefEditor();
        editor.putString(LOGGED_USER_USERNAME, loginCredentials.getUserName());
        editor.putString(LOGGED_USER_PASSWORD, loginCredentials.getPassword());
        editor.apply();
    }

    @Override
    public void removeCurrentLoggedUserData() {
        SharedPreferences.Editor editor = getSharedPrefEditor();
        editor.remove(LOGGED_USER_USERNAME);
        editor.remove(LOGGED_USER_PASSWORD);
        editor.apply();
    }

    private SharedPreferences.Editor getSharedPrefEditor() {
        SharedPreferences sharedpreferences = context.getSharedPreferences(SHARED_PREF, Context.MODE_PRIVATE);
        return sharedpreferences.edit();
    }
}
