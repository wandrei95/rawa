package com.example.andrei.rawa.android.ui;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.andrei.rawa.R;
import com.example.andrei.rawa.android.RawaApplication;
import com.example.andrei.rawa.entities.ServerError;
import com.example.andrei.rawa.presenters.login.LoginPresenter;
import com.example.andrei.rawa.presenters.login.LoginView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import toothpick.Scope;
import toothpick.Toothpick;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, LoginView {

    @BindView(R.id.username)
    EditText etUserName;
    @BindView(R.id.password)
    EditText etPassword;
    @BindView(R.id.login_button)
    Button btnLoginButton;
    @BindView(R.id.loading_spinner)
    ProgressBar pbLoadingSpinner;

    @Inject
    LoginPresenter loginPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Scope scope = Toothpick.openScope(RawaApplication.APP_SCOPE);
        Toothpick.inject(this, scope);
        loginPresenter.bindView(this);

        ButterKnife.bind(this);
        btnLoginButton.setOnClickListener(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        loginPresenter.unbindView();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.login_button:
                loginPresenter.doLogin(etUserName.getText().toString(), etPassword.getText().toString());
        }
    }

    @Override
    public void onLoginOperationStarted() {
        pbLoadingSpinner.setVisibility(View.VISIBLE);
    }

    @Override
    public void onLoginSuccess() {
        pbLoadingSpinner.setVisibility(View.GONE);
        Toast.makeText(this, "Success", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onLogoutSuccess() {

    }

    @Override
    public void onLoginOperationFailure(ServerError serverError) {

    }
}
