package com.example.andrei.rawa.entities;

public class LoginCredentials {
    private String userName;
    private String password;

    public LoginCredentials(String userName, String password) {
        this.userName = userName;
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }
}
