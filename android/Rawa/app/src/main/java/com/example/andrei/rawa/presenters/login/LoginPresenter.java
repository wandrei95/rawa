package com.example.andrei.rawa.presenters.login;

import com.example.andrei.rawa.entities.LoginCredentials;
import com.example.andrei.rawa.entities.ServerError;
import com.example.andrei.rawa.use_cases.login.Login;

import javax.inject.Inject;

public class LoginPresenter implements Login.LoginListener{
    private Login login;
    private LoginView loginView;

    @Inject
    public LoginPresenter(Login login) {
        this.login = login;
    }

    public void bindView(LoginView loginView) {
        this.loginView = loginView;
        login.bindListener(this);
    }

    public void unbindView(){
        loginView = null;
        login.unbindListener();
    }

    public void doLogin(String userName, String password){
        loginView.onLoginOperationStarted();
        login.doLogin(new LoginCredentials(userName, password));
    }

    public void doLogout(){
        loginView.onLoginOperationStarted();
        login.doLogout();
    }

    @Override
    public void onLoginSuccess() {
        loginView.onLoginSuccess();
    }

    @Override
    public void onLogoutSuccess() {
        loginView.onLogoutSuccess();
    }

    @Override
    public void onLoginOperationFailure(ServerError serverError) {
        loginView.onLoginOperationFailure(serverError);
    }
}
