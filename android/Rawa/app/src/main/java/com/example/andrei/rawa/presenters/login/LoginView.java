package com.example.andrei.rawa.presenters.login;

import com.example.andrei.rawa.entities.ServerError;

public interface LoginView {
    void onLoginOperationStarted();

    void onLoginSuccess();

    void onLogoutSuccess();

    void onLoginOperationFailure(ServerError serverError);
}
