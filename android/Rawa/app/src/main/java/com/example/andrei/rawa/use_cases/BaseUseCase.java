package com.example.andrei.rawa.use_cases;


import com.example.andrei.rawa.entities.ErrorCode;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseUseCase<PARAM, RESULT> implements UseCaseOperation.UseCaseOperationListener<RESULT> {
    protected List<UseCaseListener<RESULT>> listeners = new ArrayList<>();
    private UseCaseOperation<PARAM, RESULT> useCaseOperation;

    public BaseUseCase(UseCaseOperation<PARAM, RESULT> useCaseOperation) {
        this.useCaseOperation = useCaseOperation;
    }

    public void bindListener(UseCaseListener<RESULT> listener) {
        listeners.add(listener);
        useCaseOperation.bindListener(this);
    }

    public void unbindAllListeners() {
        listeners.clear();
        useCaseOperation.unbindListener();
    }

    public void unbindListener(UseCaseListener<RESULT> listener) {
        listeners.remove(listener);
        useCaseOperation.unbindListener();
    }

    public void executeUseCaseOperation(PARAM param) {
        useCaseOperation.start(param);
    }

    @Override
    public void onOperationSuccess(RESULT RESULT) {
        for (UseCaseListener<RESULT> listener : listeners) {
            listener.onOperationSuccess(RESULT);
        }
    }

    @Override
    public void onOperationFailure(ErrorCode errorCode) {
        for (UseCaseListener<RESULT> listener : listeners) {
            listener.onOperationFailure(errorCode);
        }
    }

    public interface UseCaseListener<RESULT> {
        void onOperationSuccess(RESULT result);

        void onOperationFailure(ErrorCode errorCode);
    }
}
