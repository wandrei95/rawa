package com.example.andrei.rawa.use_cases;

import com.example.andrei.rawa.entities.LoginCredentials;

//TODO break into more classes later
public interface LoggedUserDataStorage {
    void saveSessionID(String sessionID);

    void removeCurrentSessionID();

    void saveCurrentLoggedUserData(LoginCredentials loginCredentials);

    void removeCurrentLoggedUserData();
}
