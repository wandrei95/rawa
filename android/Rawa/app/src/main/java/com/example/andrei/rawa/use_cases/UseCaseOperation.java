package com.example.andrei.rawa.use_cases;

import com.example.andrei.rawa.entities.ErrorCode;

public interface UseCaseOperation<PARAMS, RESULT> {
    void start(PARAMS params);

    void unbindListener();

    void bindListener(UseCaseOperationListener<RESULT> listener);

    interface UseCaseOperationListener<RESULT> {
        void onOperationSuccess(RESULT result);

        void onOperationFailure(ErrorCode errorCode);
    }

}
