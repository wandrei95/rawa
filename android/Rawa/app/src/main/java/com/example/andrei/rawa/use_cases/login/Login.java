package com.example.andrei.rawa.use_cases.login;

import com.example.andrei.rawa.entities.ServerError;
import com.example.andrei.rawa.entities.LoginCredentials;
import com.example.andrei.rawa.use_cases.LoggedUserDataStorage;

import javax.inject.Inject;

public class Login implements LoginApiListener {
    private LoginApi loginApi;
    private LoggedUserDataStorage loggedUserDataStorage;
    private LoginListener listener;

    @Inject
    public Login(LoginApi loginApi, LoggedUserDataStorage loggedUserDataStorage) {
        this.loginApi = loginApi;
        this.loggedUserDataStorage = loggedUserDataStorage;
    }

    public void bindListener(LoginListener listener) {
        this.listener = listener;
    }

    public void unbindListener() {
        listener = null;
    }

    public void doLogin(LoginCredentials loginCredentials) {
        loginApi.doLogin(loginCredentials, this);
    }

    public void doLogout() {
        loginApi.doLogout(this);
    }

    @Override
    public void onLoginSuccess(LoginCredentials loginCredentials, String sessionID) {
        if (listener != null) {
            loggedUserDataStorage.saveSessionID(sessionID);
            loggedUserDataStorage.saveCurrentLoggedUserData(loginCredentials);
            listener.onLoginSuccess();
        }
    }

    @Override
    public void onLogoutSuccess() {
        if (listener != null) {
            loggedUserDataStorage.removeCurrentSessionID();
            loggedUserDataStorage.removeCurrentLoggedUserData();
            listener.onLogoutSuccess();
        }
    }

    @Override
    public void onLoginOperationFailure(ServerError serverError) {
        if (listener != null) {
            listener.onLoginOperationFailure(serverError);
        }
    }

    public interface LoginListener {
        void onLoginSuccess();

        void onLogoutSuccess();

        void onLoginOperationFailure(ServerError serverError);
    }
}
