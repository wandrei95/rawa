package com.example.andrei.rawa.use_cases.login;

import com.example.andrei.rawa.entities.LoginCredentials;

public interface LoginApi {
    void doLogin(LoginCredentials loginCredentials, LoginApiListener listener);

    void doLogout(LoginApiListener listener);
}