package com.example.andrei.rawa.use_cases.login;

import com.example.andrei.rawa.entities.LoginCredentials;
import com.example.andrei.rawa.entities.ServerError;

public interface LoginApiListener {
    void onLoginSuccess(LoginCredentials loginCredentials, String sessionID);

    void onLogoutSuccess();

    void onLoginOperationFailure(ServerError serverError);
}