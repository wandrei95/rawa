<?php

require "ApiResponseErrorEnum.php";

class ApiResponse implements JsonSerializable
{
    private $errorCode;

    public function setErrorCode($errorCode)
    {
        if (!ApiResponseErrorEnum::isValidValue($errorCode)) {
            throw new InvalidArgumentException("error code not defined");
        }
        $this->errorCode = $errorCode;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}

?>