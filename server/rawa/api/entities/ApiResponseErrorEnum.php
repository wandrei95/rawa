<?php

require "BasicEnum.php";

abstract class ApiResponseErrorEnum extends BasicEnum
{
    const NO_ERROR = 0;
    const INVALID_CREDENTIALS = 1;
    const NO_SESSION_ID = 2;
    const PASSWORD_TOO_SHORT = 3;
    const USERNAME_EXISTS = 4;
}

?>