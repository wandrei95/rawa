<?php

class Lesson implements JsonSerializable
{
    private $lessonId;
    private $name;
    private $technologyId;
    private $lessonContent;

    public function __construct($id, $name, $technologyId)
    {
        $this->setLessonId($id);
        $this->setName($name);
        $this->setTechnologyId($technologyId);
    }

    public static function createLessonWithContent($id, $name, $technologyId, $lessonContent)
    {
        $lesson = new Lesson($id, $name, $technologyId);
        $lesson->setLessonContent($lessonContent);
        return $lesson;
    }

    public function setLessonId($lessonId)
    {
        if (!is_int($lessonId)) {
            throw new InvalidArgumentException("id must be a integer");
        }
        $this->lessonId = $lessonId;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function setTechnologyId($technologyId)
    {
        if (!is_int($technologyId)) {
            throw new InvalidArgumentException("technoloy id must be a integer");
        }
        $this->technologyId = $technologyId;
    }

    public function getLessonContent()
    {
        return $this->lessonContent;
    }

    public function setLessonContent($lessonContent)
    {
        $this->lessonContent = $lessonContent;
    }

    public function getLessonId()
    {
        return $this->lessonId;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getTechnologyId()
    {
        return $this->technologyId;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}

?>