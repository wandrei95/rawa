<?php

class LessonTest implements JsonSerializable
{
    private $testId;
    private $testName;
    private $testContent;
    private $answers;
    private $correctAnswerId;
    private $technologyId;
    private $lessonId;

    public function setTestId($testId)
    {
        if (!is_int($testId)) {
            throw new InvalidArgumentException("test id must be a integer");
        }
        $this->testId = $testId;
    }

    public function setTestName($testName)
    {
        $this->testName = $testName;
    }

    public function setTestContent($testContent)
    {
        $this->testContent = $testContent;
    }

    public function setAnswers($answers)
    {
        $this->answers = $answers;
    }

    public function setCorrectAnswerId($correctAnswerId)
    {
        if (!is_int($correctAnswerId)) {
            throw new InvalidArgumentException("correct answer id must be a integer");
        }
        $this->correctAnswerId = $correctAnswerId;
    }

    public function getTechnologyId()
    {
        return $this->technologyId;
    }

    public function setTechnologyId($technologyId)
    {
        $this->technologyId = $technologyId;
    }

    public function getLessonId()
    {
        return $this->lessonId;
    }

    public function setLessonId($lessonId)
    {
        $this->lessonId = $lessonId;
    }

    public function getTestId()
    {
        return $this->testId;
    }

    public function getTestName()
    {
        return $this->testName;
    }

    public function getTestContent()
    {
        return $this->testContent;
    }

    public function getAnswers()
    {
        return $this->answers;
    }

    public function getCorrectAnswerId()
    {
        return $this->correctAnswerId;
    }


    function jsonSerialize()
    {
        return get_object_vars($this);
    }
}

?>