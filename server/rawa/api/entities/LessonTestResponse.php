<?php

class LessonTestResponse implements JsonSerializable
{
    private $answerId;
    private $answerText;

    public function __construct($id, $answerText)
    {
        $this->setAnswerId($id);
        $this->setText($answerText);
    }

    public function setAnswerId($answerId)
    {
        if (!is_int($answerId)) {
            throw new InvalidArgumentException("id must be a integer");
        }
        $this->answerId = $answerId;
    }

    public function setText($answerText)
    {
        $this->answerText = $answerText;
    }

    function jsonSerialize()
    {
        return get_object_vars($this);
    }
}

?>