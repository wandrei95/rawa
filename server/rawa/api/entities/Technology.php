<?php

class Technology implements JsonSerializable
{
    private $id;
    private $name;

    public static function createInstance($id, $name)
    {
        $technology = new Technology();
        $technology->setId($id);
        $technology->setName($name);
        return $technology;
    }

    public function setId($id)
    {
        if (!is_int($id)) {
            throw new InvalidArgumentException("id must be a integer");
        }
        $this->id = $id;
    }

    public function setName($name)
    {
        if (!is_string($name)) {
            throw new InvalidArgumentException("name must be a string");
        }
        $this->name = $name;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}