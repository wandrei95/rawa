<?php
require '../entities/ApiResponse.php';
require '../repository/TechnologiesRepository.php';

session_start();

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $response = new ApiResponse();
    if (isset($_SESSION['userId'])) {
        $inputJSON = file_get_contents('php://input');
        $input = json_decode($inputJSON, TRUE);

        $lessonName = $input['lessonName'];
        $lessonContent = $input['lessonContent'];
        $technologyId = $input['technologyId'];

        $technologiesRepository = new TechnologiesRepository();

        $technologiesRepository->addLesson($lessonName, $lessonContent, $technologyId);
        $response->setErrorCode(ApiResponseErrorEnum::NO_ERROR);
    } else {
        $response->setErrorCode(ApiResponseErrorEnum::NO_SESSION_ID);
    }

    echo json_encode($response);
}
?>