<?php
require '../entities/ApiResponse.php';
require '../repository/TechnologiesRepository.php';

session_start();

if ($_SERVER["REQUEST_METHOD"] == "GET") {
    header("Content-Type: application/json");
    $v = json_decode(stripslashes(file_get_contents("php://input")));
    $v = json_decode(stripslashes($_GET["data"]));

    $response = new ApiResponse();
    if (isset($_SESSION['userId'])) {
        $technologiesRepository = new TechnologiesRepository();

        $lessons = $technologiesRepository->getLessons($v->technologyId);
        $response->lessons = $lessons;

        $response->setErrorCode(ApiResponseErrorEnum::NO_ERROR);
    } else {
        $response->setErrorCode(ApiResponseErrorEnum::NO_SESSION_ID);
    }

    echo json_encode($response);
}
?>