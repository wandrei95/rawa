<?php
require '../../entities/ApiResponse.php';
require '../../repository/TechnologiesRepository.php';

session_start();

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $response = new ApiResponse();
    if (isset($_SESSION['userId'])) {
        $inputJSON = file_get_contents('php://input');
        $input = json_decode($inputJSON, TRUE);

        $technologyId = $input['technologyId'];
        $lessonId = $input['lessonId'];
        $testName = $input['testName'];
        $correctAnswerId = $input['correctAnswerId'];
        $testContent = $input['testContent'];
        $answers = $input['answers'];

        $technologiesRepository = new TechnologiesRepository();

        echo $technologiesRepository->addTest($technologyId, $lessonId, $testName, $correctAnswerId, $testContent, $answers);
        $response->setErrorCode(ApiResponseErrorEnum::NO_ERROR);
    } else {
        $response->setErrorCode(ApiResponseErrorEnum::NO_SESSION_ID);
    }

    echo json_encode($response);
}
?>