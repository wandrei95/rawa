<?php
require '../entities/ApiResponse.php';
require '../repository/TechnologiesRepository.php';

session_start();

if ($_SERVER["REQUEST_METHOD"] == "PUT") {
    $response = new ApiResponse();
    if (isset($_SESSION['userId'])) {
        $inputJSON = file_get_contents('php://input');
        $input = json_decode($inputJSON, TRUE);

        $lessonId = (int)$input['lessonId'];
        $lesson = $input['lesson'];

        $technologiesRepository = new TechnologiesRepository();

        $technologiesRepository->updateLesson($lessonId, $lesson);

        $response->setErrorCode(ApiResponseErrorEnum::NO_ERROR);
    } else {
        $response->setErrorCode(ApiResponseErrorEnum::NO_SESSION_ID);
    }

    echo json_encode($response);
}
?>