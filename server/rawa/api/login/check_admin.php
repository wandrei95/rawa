<?php
require "../repository/LoginRepository.php";
require "../entities/ApiResponse.php";

session_start();

if ($_SERVER["REQUEST_METHOD"] == "GET") {
    $response = new ApiResponse();
    if (isset($_SESSION['userId'])) {
        $loginRepository = new LoginRepository();

        $isAdmin = $loginRepository->isCurrentUserAdmin($_SESSION['userId']);
        $response->isAdmin = $isAdmin;

        $response->setErrorCode(ApiResponseErrorEnum::NO_ERROR);
    } else {
        $response->setErrorCode(ApiResponseErrorEnum::NO_SESSION_ID);
    }

    echo json_encode($response);
}
?>