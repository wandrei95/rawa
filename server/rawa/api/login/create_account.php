<?php
require "../repository/LoginRepository.php";
require "../entities/ApiResponse.php";

session_start();
$sessionId = session_id();

$username = $password = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $inputJSON = file_get_contents('php://input');
    $input = json_decode($inputJSON, TRUE);

    $username = test_input($input['username']);
    $password = test_input($input['password']);

    $loginRepository = new LoginRepository();
    $response = new ApiResponse();

    if (strlen($password) < 4) {
        $response->setErrorCode(ApiResponseErrorEnum::PASSWORD_TOO_SHORT);
        echo json_encode($response);
    } else {
        $accountCreatedSuccessfully = $loginRepository->createAccount($username, $password);
        if ($accountCreatedSuccessfully) {
            $response->setErrorCode(ApiResponseErrorEnum::NO_ERROR);
            echo json_encode($response);
        } else {
            $response->setErrorCode(ApiResponseErrorEnum::USERNAME_EXISTS);
            echo json_encode($response);
        }
    }
}


function test_input($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

?>