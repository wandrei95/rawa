<?php
require "../repository/LoginRepository.php";
require "../entities/ApiResponse.php";

session_start();
$sessionId = session_id();

$username = $password = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $inputJSON = file_get_contents('php://input');
    $input = json_decode($inputJSON, TRUE);

    $username = test_input($input['username']);
    $password = test_input($input['password']);

    $loginRepository = new LoginRepository();
    $response = new ApiResponse();

    $userId = $loginRepository->logUser($username, $password);
    if ($userId > 0) {
        $response->setErrorCode(ApiResponseErrorEnum::NO_ERROR);
        $response->sessionId = $sessionId;
        $_SESSION['userId'] = $userId;
        $loginRepository->setSessionIdForUser($userId, $sessionId);
        echo json_encode($response);
    } else {
        $response->setErrorCode(ApiResponseErrorEnum::INVALID_CREDENTIALS);
        echo json_encode($response);
    }
}


function test_input($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

?>