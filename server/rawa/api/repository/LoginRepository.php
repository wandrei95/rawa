<?php

require 'Repository.php';

class LoginRepository extends Repository
{
    public function logUser($username, $password)
    {
        $conn = $this->getDbConnection();

        $sql = "SELECT id, username, password FROM users WHERE username = '$username' AND password = '$password'";
        $result = $conn->query($sql);

        $row = $result->fetch_assoc();

        $conn->close();
        return ((int)$row["id"]);
    }

    public function createAccount($username, $password)
    {
        $conn = $this->getDbConnection();

        $sql = "INSERT INTO users (username, password) VALUES ('$username', '$password')";
        $result = $conn->query($sql);

        $conn->close();

        return $result;
    }

    public function setSessionIdForUser($id, $sessionId)
    {
        $conn = $this->getDbConnection();

        $sql = "UPDATE users SET sessionId = '$sessionId' WHERE id =  $id";
        $conn->query($sql);

        $conn->close();
    }

    public function isCurrentUserAdmin($currentUserId)
    {
        $conn = $this->getDbConnection();

        $sql = "SELECT isAdmin FROM users WHERE id = $currentUserId";
        $result = $conn->query($sql);
        $row = $result->fetch_assoc();

        $isAdmin = (int)$row["isAdmin"];

        $conn->close();

        if ($isAdmin == 0) {
            return true;
        } else {
            return false;
        }
    }
}