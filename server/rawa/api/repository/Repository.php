<?php

class Repository
{
    private $servername = "localhost";
    private $dbUsername = "root";
    private $dbPassword = "andrei";
    private $dbname = "rawa";

    protected function getDbConnection() {
        $conn = new mysqli($this->servername, $this->dbUsername, $this->dbPassword, $this->dbname);

        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }

        return $conn;
    }
}

?>