<?php

require 'Repository.php';
require(dirname(__FILE__) . "/../entities/LessonTest.php");
require(dirname(__FILE__) . "/../entities/Technology.php");
require(dirname(__FILE__) . "/../entities/Lesson.php");

class TechnologiesRepository extends Repository
{
    //technologies
    public function getTechnologies()
    {
        $conn = $this->getDbConnection();

        $sql = "SELECT id, name FROM technologies";
        $result = $conn->query($sql);
        $success = $result->num_rows > 0;

        $conn->close();
        if ($success) {
            $technologies = array();
            $i = 0;
            while ($row = $result->fetch_assoc()) {
                $technologies[$i++] = Technology::createInstance((int)$row["id"], $row["name"]);
            }
            return $technologies;
        }
    }

    public function updateTechnology($technologyId, Technology $technology)
    {
        $conn = $this->getDbConnection();

        $sql = "UPDATE technologies SET name = " . $technology->getName()
            . "WHERE id = " . $technologyId;
        $conn->query($sql);

        $conn->close();

        return $technology;
    }

    public function deleteTechnology($technologyId)
    {
        $this->deleteTestsByTechnologyId($technologyId);
        $this->deleteLessonsByTechnologyId($technologyId);

        $conn = $this->getDbConnection();

        $sql = "DELETE FROM technologies WHERE id = " . $technologyId;

        $conn->query($sql);

        $conn->close();
    }

    public function addTechnology($technologyName) {
        $conn = $this->getDbConnection();

        $sql = "INSERT INTO technologies (name) VALUES ('$technologyName')";
        $result = $conn->query($sql);

        $conn->close();

        return $result;
    }

    //lessons
    public function getLessons($technologyId)
    {
        $conn = $this->getDbConnection();

        $sql = "SELECT * FROM lessons WHERE technologyId = " . $technologyId;
        $result = $conn->query($sql);
        $success = $result->num_rows > 0;

        $conn->close();
        if ($success) {
            $lessons = array();
            $i = 0;
            while ($row = $result->fetch_assoc()) {
                $lessons[$i++] = new Lesson((int)$row["lessonId"], $row["name"], (int)$row["technologyId"]);
            }
            return $lessons;
        }

        $conn->close();
    }

    public function getLesson($lessonId)
    {
        $conn = $this->getDbConnection();

        $sql = "SELECT name, lessonContent, technologyId FROM lessons WHERE lessonId = " . $lessonId;
        $result = $conn->query($sql);

        $conn->close();

        $row = $result->fetch_assoc();
        $name = $row["name"];
        $lessonContent = $row["lessonContent"];
        $technologyId = (int)$row["technologyId"];

        return Lesson::createLessonWithContent($lessonId, $name, $technologyId, $lessonContent);
    }

    public function updateLesson($lessonId, Lesson $lesson)
    {
        $conn = $this->getDbConnection();

        $sql = "UPDATE lessons SET name = " . $lesson->getName()
            . "lessonContent = " . $lesson->getLessonContent()
            . "technologyId = " . $lesson->getTechnologyId()
            . "WHERE lessonId = " . $lessonId;
        $conn->query($sql);

        $conn->close();

        return $lesson;
    }

    public function deleteLesson($lessonId)
    {
        $this->deleteTestsByLessonId($lessonId);

        $conn = $this->getDbConnection();

        $sql = "DELETE FROM lessons WHERE lessonId = " . $lessonId;

        $conn->query($sql);

        $conn->close();
    }

    public function deleteLessonsByTechnologyId($technologyId)
    {
        $conn = $this->getDbConnection();

        $sql = "DELETE FROM lessons WHERE technologyId = " . $technologyId;

        $conn->query($sql);

        $conn->close();
    }

    public function addLesson($lessonName, $lessonContent, $technologyId) {
        $conn = $this->getDbConnection();

        $sql = "INSERT INTO lessons (name, lessonContent, technologyId) VALUES ('$lessonName', '$lessonContent', '$technologyId')";
        $result = $conn->query($sql);

        $conn->close();

        return $result;
    }

    //tests
    public function getLessonTests($lessonId, $technologyId)
    {
        $conn = $this->getDbConnection();

        $sql = "SELECT * FROM tests WHERE technologyId = "
            . $technologyId . " AND " . "lessonId = " . $lessonId;
        $result = $conn->query($sql);
        $success = $result->num_rows > 0;

        $conn->close();
        if ($success) {
            $tests = array();
            $i = 0;
            while ($row = $result->fetch_assoc()) {
                $lessonTest = new LessonTest();

                $lessonTest->setTestId((int)$row["testId"]);
                $lessonTest->setCorrectAnswerId((int)$row["correctAnswerId"]);
                $testName = $row["testName"];
                $lessonTest->setTestName($testName);
                $lessonTest->setLessonId((int)$row["lessonId"]);
                $lessonTest->setTechnologyId((int)$row["technologyId"]);

                $lessonTest->setTestContent($row["testContent"]);

                $answersJson = $row["answers"];
                $answersArray = json_decode($answersJson, true);

                $lessonTest->setAnswers($answersArray);

                $tests[$i++] = $lessonTest;
            }
            return $tests;
        }
    }

    public function updateTest($testId, LessonTest $test)
    {
        $conn = $this->getDbConnection();

        $sql = "UPDATE tests SET technologyId = " . $test->getTechnologyId()
            . "lessonId = " . $test->getLessonId()
            . "testName = " . $test->getTestName()
            . "correctAnswerId = " . $test->getCorrectAnswerId()
            . "testContent = " . $test->getTestContent()
            . "answers = " . $test->getAnswers()
            . "WHERE testId = " . $testId;
        $conn->query($sql);

        $conn->close();

        return $test;
    }

    public function deleteTest($testId)
    {
        $conn = $this->getDbConnection();

        $sql = "DELETE FROM tests WHERE testId = " . $testId;

        $conn->query($sql);

        $conn->close();
    }

    public function deleteTestsByLessonId($lessonId)
    {
        $conn = $this->getDbConnection();

        $sql = "DELETE FROM tests WHERE lessonId = " . $lessonId;

        $conn->query($sql);

        $conn->close();
    }

    public function deleteTestsByTechnologyId($technologyId)
    {
        $conn = $this->getDbConnection();

        $sql = "DELETE FROM tests WHERE technologyId = " . $technologyId;

        $conn->query($sql);

        $conn->close();
    }

    public function addTest($technologyId, $lessonId, $testName, $correctAnswerId, $testContent, $answers) {
        $conn = $this->getDbConnection();

        $answersJson = json_encode($answers);

        $sql = "INSERT INTO tests (technologyId, lessonId, testName, correctAnswerId, testContent, answers) VALUES ('$technologyId', '$lessonId', '$testName', '$correctAnswerId', '$testContent', '$answersJson')";
        $result = $conn->query($sql);

        $conn->close();

        return $sql ;
    }
}

?>