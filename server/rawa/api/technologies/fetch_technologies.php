<?php
require '../entities/ApiResponse.php';
require '../repository/TechnologiesRepository.php';

session_start();

if ($_SERVER["REQUEST_METHOD"] == "GET") {
    $response = new ApiResponse();
    $response = new ApiResponse();
    if (isset($_SESSION['userId'])) {
        $technologiesRepository = new TechnologiesRepository();

        $technologies = $technologiesRepository->getTechnologies();
        $response->technologies = $technologies;

        $response->setErrorCode(ApiResponseErrorEnum::NO_ERROR);
    } else {
        $response->setErrorCode(ApiResponseErrorEnum::NO_SESSION_ID);
    }

    echo json_encode($response);
}
?>